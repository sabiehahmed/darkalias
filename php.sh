#Composer
alias c='composer'
alias cr='composer require'
alias cda='composer dumpautoload --optimize'
alias cu='composer update'

#PHPUnit
t() {
  if [ -f vendor/bin/phpunit ]; then
    vendor/bin/phpunit "$@"
  else
    phpunit "$@"
  fi
}