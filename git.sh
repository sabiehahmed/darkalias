#Git.
alias g='git'

#Git status
alias gs='git status'

#Git Log.
alias gl="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

#Git reseta
alias gnah='git reset --hard;git clean -df'

#Git add and commit
alias gc="git commit -a"

#Git push
alias gpush='git push origin'

#Git Pull origin
alias gpull='git pull origin'

#Opens PR for current branch
function openpr() {
  br=`git branch | grep "*"`
  remote=`git remote -v | grep "origin"`
  repo=$1
  parentBranch=$2
  google-chrome  ${remote}/${repo/* /}/compare/${parentBranch/* /}:${br/* /}\?expand\=1
}