#Laravel
alias lara-inst='composer create-project --prefer-dist laravel/laravel'


#Artisan
alias art="php artisan"
alias migrate="php artisan migrate"
alias migraterf="php artisan migrate:refresh"
alias migraterf="php artisan migrate:refresh"
alias serve="php artisan serve"
alias tinker='php artisan tinker'


